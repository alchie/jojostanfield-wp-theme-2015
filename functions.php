<?php

require('inc/bootstrap3-menu-walker.php');

if ( ! function_exists( 'jojo2015_setup' ) ) :

function jojo2015_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on jojo2015, use a find and replace
	 * to change 'jojo2015' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'jojo2015', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'jojo2015' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

}
endif; // jojo2015_setup
add_action( 'after_setup_theme', 'jojo2015_setup' );

function jojo2015_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'jojo2015' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'jojo2015' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'jojo2015_widgets_init' );

function jojo2015_scripts() {

	wp_enqueue_style( 'OpenSans-font', 'http://fonts.googleapis.com/css?family=Open+Sans:400,300,700');
	wp_enqueue_style( 'OpenSansCondensed-font', 'http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300');
	
	// Add Bootstrap, used in the main stylesheet.
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.1.0' );
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '20150210' );
	//wp_enqueue_style( 'bootstrap-cdn-css', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css', array(), '3.3.2' );
	//wp_enqueue_script( 'bootstrap-cdn-js', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js', array(), '20150210' );
	
	// Load our main stylesheet.
	wp_enqueue_style( 'jojo2015-style', get_stylesheet_uri() );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'jojo2015-ie', get_template_directory_uri() . '/css/ie.css', array( 'jojo2015-style' ), '20141010' );
	wp_style_add_data( 'jojo2015-ie', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'jojo2015-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'jojo2015-style' ), '20141010' );
	wp_style_add_data( 'jojo2015-ie7', 'conditional', 'lt IE 8' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_script( 'jojo2015-script', get_template_directory_uri() . '/js/scripts.js', array( 'jquery', 'bootstrap-js' ), '20141212', true );

}
add_action( 'wp_enqueue_scripts', 'jojo2015_scripts' );

