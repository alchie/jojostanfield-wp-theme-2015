<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<script>(function(){document.documentElement.className='js'})();</script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="wrap">
  
<header id="masthead" class="site-header" role="banner">
    <div class="container">
    <div class="row">
      <div class="col-sm-12">
			<div class="site-branding">
				<?php
					if ( is_front_page() && is_home() ) : ?>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php else : ?>
						<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php endif;

					$description = get_bloginfo( 'description', 'display' );
					if ( $description || is_customize_preview() ) : ?>
						<p class="site-description"><?php echo $description; ?></p>
					<?php endif;
				?>
			</div><!-- .site-branding -->
      </div>
    </div>
    </div>
</header><!-- .site-header -->
  
<!-- Fixed navbar -->

<?php
$item_wrap = '
    <div class="container">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand visible-xs" href="#">Navigation</a>
  </div>
  	<div class="pull-right visible-lg" id="more-contact-details"><i class="glyphicon glyphicon-envelope"></i> <a href="">Email</a> or call me at:
	<i class="glyphicon glyphicon-earphone"></i> <span class="phone">805.937.1764</span></div>
  <div class="collapse navbar-collapse navbar-ex1-collapse" id="bs-example-navbar-collapse-1">

	<ul id="%1$s" class="%2$s">
		%3$s
		
	</ul>
  </div>
  
      </div><!--/.container -->
';

 wp_nav_menu( array(
    'theme_location'    => 'primary',
    'container'     => 'nav',
    'container_id'      => 'primary',
    'container_class'   => 'navbar navbar-custom navbar-inverse navbar-static-top',
    'menu_class'        => 'nav navbar-nav navbar-left', 
    'echo'          => true,
    'items_wrap'        => $item_wrap,
    'depth'         => 10, 
    'walker'        => new Bootstrap3_Walker_Nav_Menu
) );
 ?>

<!-- Begin page content -->
<div class="divider" id="section1"></div>