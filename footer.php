
<div id="real-estate-needs">
<div class="container">
  	<div class="col-sm-12 text-center">
      <h2>How Can I Help With Your Real Estate Needs?</h2>

      <ul class="list-inline center-block icon-links">
        <li><a href="#" class="find_a_home">Find a Home</a></li>
        <li><a href="#" class="sell_a_home">Sell a Home</a></li>
        <li><a href="#" class="relocate">Relocate</a></li>
        <li><a href="#" class="financing_info">Financing Info</a></li>
      </ul>
      
  	</div><!--/col-->
</div><!--/container-->
</div>

</div><!--/wrap-->

<div id="footer">
  <div class="container">
		<div class="col-md-6">
			<p><a href="">Equal Housing Opportunity</a> | <a href="">Privacy Policy</a></p>
			<p>All content &copy; Homes Central Coast 2013. All Rights Reserved</p>
		</div>
		<div class="col-md-6 text-right">
			<p>A portion of our commission goes to:</p>
			<p><img src="<?php echo  get_template_directory_uri() . '/images/charity_icon.png'; ?>"></p>
		</div>
  </div>
</div>

<?php wp_footer(); ?>

</body>
</html>
